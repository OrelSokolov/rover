class Field
  attr_reader :x, :y

  def initialize(x, y)
    @x, @y = x.to_i, y.to_i
    @rovers = []
  end

  def add_rover(rover)
    @rovers << rover
  end

  def position_free?(x, y)
    @rovers.none?{ |r| r.x == x && r.y == y }
  end

  def in_range?(x,y)
    x <= @x && y <= @y
  end

  def can_move_to_position?(x, y)
    in_range?(x, y) && position_free?(x, y)
  end

  def to_s
    "Field #{x}x#{y}"
  end
end