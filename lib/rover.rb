class Rover
  attr_reader :x, :y
  DIRECTIONS_MAP = { 'N' => { y: 1 }, 'E' => { x: 1 }, 'S' => { y: -1 }, 'W' => { x: -1 } }

  def initialize(x, y, direction, field)
    @x, @y, @field = x.to_i, y.to_i, field
    @directions = ['N', 'E', 'S', 'W']
    while current_direction != direction
      @directions = @directions.rotate
    end

    raise ArgumentError, "Position already acquired by other rover" unless field.position_free?(@x, @y)
    raise ArgumentError, "Position out of range" unless field.in_range?(@x, @y)
    @field.add_rover(self)
  end

  def execute!(cmd)
    case cmd
      when 'L'
        rotate('L')
      when 'R'
        rotate('R')
      when 'M'
        move!
      else
        raise RuntimeError, "Unknown command: #{cmd}"
    end
  end

  def current_direction
    @directions.first
  end

  def move!
    axis = DIRECTIONS_MAP[current_direction].keys.first
    @next_x, @next_y = @x, @y
    step_value = DIRECTIONS_MAP[current_direction][axis]
    cmd = "@next_#{axis.to_s} = @next_#{axis.to_s} + #{step_value.to_s}"
    eval(cmd)
    if @field.can_move_to_position?(@next_x, @next_y)
      @x, @y = @next_x, @next_y
    else
      raise RuntimeError, "Can not move to position, position acquired by other rover"
    end
  end

  def rotate(direction)
    if direction == 'R'
      @directions = @directions.rotate(1)
    elsif direction == 'L'
      @directions = @directions.rotate(-1)
    else
      raise ArgumentError, 'Unknown direction'
    end
  end

  def to_s
    "#{@x} #{@y} #{current_direction}"
  end

end