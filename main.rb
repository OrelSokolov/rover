require_relative 'lib/field'
require_relative 'lib/rover'

puts "Enter field dimensions"
coordinates = gets.chomp.split
field = Field.new(*coordinates)
puts field
puts "Enter first rover coordinates"
arguments = gets.chomp.split
arguments << field
rover1 = Rover.new(*arguments)
puts "Enter commands!"
commands = gets.chomp
commands.each_char do |cmd|
  rover1.execute!(cmd)
end
puts rover1
puts "Enter second rover coordinates"
arguments = gets.chomp.split
arguments << field
rover2 = Rover.new(*arguments)
puts "Enter commands!"
commands = gets.chomp
commands.each_char do |cmd|
  rover2.execute!(cmd)
end
puts rover2
